<?php include __DIR__ . '/__connect_db.php';

$page_name = 'member-order';
$where = " WHERE 1 ";
$airline = isset($_GET['airline_name']) ? $_GET['airline_name'] : '';
$seat = isset($_GET['seat']) ? $_GET['seat'] : '';
$depature = isset($_GET['depature']) ? $_GET['depature'] : '';
$arrival = isset($_GET['arrival']) ? $_GET['arrival'] : '';

$go = '';
$back = '';
if (isset($_GET['go']) && isset($_GET['back'])) {
    $go = $_GET['go'];	$back = $_GET['back'];
}


$go_param = '';
if(! empty($go)){
    switch ($go){
        case $go:
            $where .= "AND `flight_time` >= '$go'";
            break;
    }
    $go_param = '&flight>='. $go ;
}

$back_param = '';
if(! empty($back)) {
    switch ($back) {
        case $back:
            $where .= "AND `flight_time` <= '$back'";
            break;
    }
    $back_param = '&flight<=' . $back;

}

$airline_param = '';
if(! empty($airline)) {
    switch ($airline) {
        case 'CI':
            $where .= " AND `airline` = '中華航空' ";
            break;
        case 'BR':
            $where .= " AND `airline` = '長榮航空' ";
            break;

    }
//    $airline_param = '&airline=' . $airline;
}
$seat_param='';
if(! empty($seat)) {
switch ($seat) {
    case 'first':
        $where .= " AND `class` = '頭等艙' ";
        break;
    case 'economic':
        $where .= " AND `class`= '經濟艙' ";
        break;

}}

$depature_param='';
if(! empty($depature)) {
    switch ($depature) {
        case 'ty':
            $where .= " AND `depature` = '桃園國際機場' ";
            break;
        case 'tp':
            $where .= " AND `depature`= '台北松山機場' ";
            break;
        case 'hg_port':
            $where .= " AND `depature`= '赫爾辛基機場' ";
            break;
    }}


$arrival_param='';
if(! empty($arrival)) {
    switch ($arrival) {
        case 'hg_port':
            $where .= " AND `arrival` = '赫爾辛基機場' ";
            break;
        case 'sg_port':
            $where .= " AND `arrival`= '斯德哥爾摩機場' ";
            break;
    }}


$sql= "SELECT * FROM `airline` $where";
echo "$sql <br>";
$result = $mysqli->query($sql);
echo print_r($result) ;
$row = $result ->fetch_assoc();


?>
<?php include __DIR__ . '/__html_head.php' ?>
<div class="container">
    <?php include __DIR__ . '/__navbar.php' ?>
    <div class="col-lg-10 col-lg-offset-2">


        選擇時間<br>
        <form name="form1" method="get">
        <label for="go">出發日期：</label>
        <input type="date" name="go" id="go" class="travel" value="<?= $go ?>"
               min="<?= date('Y-m-d') ?>">
        <label for="back">回程日期：</label>
        <input type="date" name="back" id="back" class="travel" value="<?= $back ?>"
               max="<?= date('Y-m-d', strtotime("+1 months", time())) ?>">
        <input type="submit" class="sum" value="submit_date">
            </form>
        <br>使用者選取期間，並撈出資料表中符合其區間的班機
        <div id="ticket">
            <form><input name="ticket" value="one" type="radio">單程票<br>
                <input name="ticket" value="two" type="radio">來回票<br> ...
            </form>
        </div>
        <!--    --------------選擇航空公司---------------    -->

        選擇航空公司<br>
        <div class="col-lg-12">
            <form action="">
                <div class="form-group">
                    <select name="airline_name" id="airline_name" class="form-control">
                        <option value="">請選擇航空公司</option>
                        <option value="CI">中華航空</option>
                        <option value="BR">長榮航空</option>
                    </select>
                </div>
            </form>
        </div>
        <!--    --------------選擇艙等---------------    -->

        選擇艙等<br>
        <div class="col-lg-12">
            <form action="">
                <div class="form-group">
                    <select name="seat" id="seat" class="form-control">
                        <option value="">請選擇艙等</option>
                        <option value="economic">經濟艙</option>
                        <option value="first">頭等艙</option>
                    </select>
                </div>
            </form>
        </div>

        <!--    --------------選擇出發地---------------    -->

        選擇出發地<br>
        <div class="col-lg-12">
            <form action="">
                <div class="form-group">
                    <select name="depature" id="depature" class="form-control">
                        <option value="">請選擇出發地</option>
                        <option value="ty">桃園國際機場</option>
                        <option value="tp">台北松山機場</option>
                        <option value="hg_port">赫爾辛基機場</option>
                    </select>
                </div>
            </form>
        </div>
        <!--    --------------選擇目的地---------------    -->

        選擇目的地<br>
        <div class="col-lg-12">
            <form action="">
                <div class="form-group">
                    <select name="arrival" id="arrival" class="form-control">
                        <option value="">選擇目的地</option>
                        <option value="hg_port">赫爾辛基機場</option>
                        <option value="sg_port">斯德哥爾摩機場</option>
                    </select>
                </div>
            </form>
        </div>

        <!--    --------------顯示撈出來的航班資訊---------------    -->
        <div class="bs-example" data-example-id="striped-table"  style="margin:50px 0">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>航空公司</th>
                    <th>艙等</th>
                    <th>出發地</th>
                    <th>目的地</th>
                    <th>價格</th>
                    <th>出發日期</th>
                    <th>詳細資訊</th>
                    <th>勾選</th>
                </tr>
                </thead>
                <tbody>
                <?php $result-> data_seek(0)?>
                <?php while($row = $result->fetch_assoc()): ?>
                    <tr>
                        <td><?= $row['airline'] ?></td>
                        <td><?= $row['class'] ?></td>
                        <td><?= $row['depature'] ?></td>
                        <td><?= $row['arrival'] ?></td>
                        <td><?= $row['price'] ?></td>
                        <td><?= $row['flight_time'] ?></td>
                        <td>詳細</td>
                        <td>
                            <input type="checkbox" value="" class="take">
                        </td>
                    </tr>
                <?php endwhile; ?>
                </tbody>
            </table>
            <button type="button" class="btn btn-default check" data-sid="<?= $row['sid'] ?>"><a href="">確認</a></button>
        </div>


    </div>
</div>
<script>
    var params = <?= json_encode($_GET) ?>;
    var qs = '';


    $('#airline_name').on('change', function() {
        //alert( $(this).val() );

        qs = '?airline_name=' + $(this).val();

        if(params.go) {
            $('#go').val(params.go);
            qs += '&go>=' +$('#go').val();
        }

        if (params.seat) {
            $('#seat').val( params.seat );
            qs += '&seat=' + $('#seat').val();
        }
        if (params.depature) {
            $('#depature').val( params.depature );
            qs += '&depature=' + $('#depature').val();
        }
        if (params.arrival) {
            $('#arrival').val( params.arrival );
            qs += '&arrival=' + $('#arrival').val();
        }
        location.href = qs;
    });
    if(params.airline_name){
        $('#airline_name').val( params.airline_name );
    }
    //**********************************************************

    $('#seat').on('change', function() {
        qs = '?seat=' + $(this).val();

        if(params.go) {
            $('#go').val(params.go);
            qs += '&go>=' +$('#go').val();
        }
        if (params.airline_name) {
            $('#airline_name').val( params.airline_name );
            qs += '&airline_name=' + $('#airline_name').val();
        }
        if (params.depature) {
            $('#depature').val( params.depature );
            qs += '&depature=' + $('#depature').val();
        }
        if (params.arrival) {
            $('#arrival').val( params.arrival );
            qs += '&arrival=' + $('#arrival').val();
        }
        location.href = qs;
    });
    if(params.seat){
        $('#seat').val( params.seat );
    }


    //**********************************************************

    $('#depature').on('change', function(){
        qs = '?depature=' + $(this).val();

        if(params.go) {
            $('#go').val(params.go);
            qs += '&go>=' +$('#go').val();
        }
        if(params.airline_name){
            $('#airline_name').val( params.airline_name );
            qs += '&airline_name=' + $('#airline_name').val();
        }
        if(params.seat){
            $('#seat').val( params.seat );
            qs += '&seat=' + $('#seat').val();
        }
        if(params.arrival){
            $('#arrival').val( params.arrival );
            qs += '&arrival=' + $('#arrival').val();
        }
        location.href = qs ;
    });
    if(params.depature){
        $('#depature').val( params.depature );
    }
    //*********************************************************
    $('#arrival').on('change', function(){
        qs = '?arrival=' + $(this).val();

        if(params.go) {
            $('#go').val(params.go);
            qs += '&go>=' +$('#go').val();
        }
        if(params.airline_name){
            $('#airline_name').val( params.airline_name );
            qs += '&airline_name=' + $('#airline_name').val();
        }
        if(params.seat){
            $('#seat').val( params.seat );
            qs += '&seat=' + $('#seat').val();
        }
        if(params.depature){
            qs += '&depature=' + $('#depature').val();
        }
        location.href = qs ;
    });
    if(params.arrival){
        $('#arrival').val( params.arrival );
    }
    //**********************************************************
    $('#go').on('change', function(){
        qs = '?go>=' + $(this).val();

        if(params.go) {
            $('#go').val(params.go);
            qs += '&go>=' +$('#go').val();
        }
        if(params.airline_name){
            $('#airline_name').val( params.airline_name );
            qs += '&airline_name=' + $('#airline_name').val();
        }
        if(params.seat){
            $('#seat').val( params.seat );
            qs += '&seat=' + $('#seat').val();
        }
        if(params.depature){
            qs += '&depature=' + $('#depature').val();
        }
//        location.href = qs ;
    });
//    if(params.go){
//        $('#go').text( params.go );
//    }



    //********************確認**************************************







    $('.check').click(function(){

        var sid = $(this).attr('data-sid');
        //var qty = $(this).prev().val();



        //alert(sid+":"+qty);

        $.get('add_to_cart.php', {sid:sid}, function(data){
            console.log(data);

            calTotalQty(data);

            alert('商品已加入購物車');
        }, 'json');

    });




    // --------------取得使用者選取日期區間的值---------------
    $('.sum').click(function () {
        var z = $('#go').val();
        console.log(z);
//     location.href= location + z;
    });
    // --------------取得使用者選取日期區間的值---------------
</script>
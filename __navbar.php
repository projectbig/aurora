<?php
$page_name = isset($page_name) ? $page_name : '';
?>
<style>
    body{
        overflow-x: hidden;
    }
    .container-fluid{
        width: 100vw;
        height:100vh;
        padding:0;
    }
    .navbar{
        background-color: RGBA(54, 102, 158, .6);
    }
    .navbar ul li{
        float: left;
        width: 13%;
        height: 10%;
        text-align: center;
        line-height: 47px;
    }
    .navbar a{
        color:#fff;
    }
    .logo{
        width:60%;
        position: absolute;
        z-index: 1;
    }
</style>
<div class="navbar hidden-xs">
    <ul>
        <li class="col-lg-2"><a href="#">認識極光</a></li>
        <li class="col-lg-2"><a href="#">極光探索</a></li>
        <li class="col-lg-2"><a href="#">極光攝影</a></li>
        <li class="col-lg-2"><a href="#">旅程回顧</a></li>
        <li class="col-lg-2"><a href="member-order.php">行程購買</a></li>
        <li class="col-lg-2"><a href="#">會員專區</a></li>
        <li class="col-lg-2"><img src="img/logo_1201-01.png" class="img-responsive logo"></a></li>
    </ul>
</div>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.css">

    <script src="js/jquery-3.1.0.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <style>
        *{
            padding: 0;
            margin: 0;
            list-style: none;
        }
        body{
            overflow-x: hidden;
        }
        .container-fluid{
            padding:0;
        }
        section{
            height: 100vh;
            width: 100vw;
        }
        .section1{
            background: url("img/首頁大圖.jpg") no-repeat 50% 50%;
            /*-webkit-background-size:auto;*/
            /*background-size:auto;*/
        }
        .navbar{
            background-color: RGBA(54, 102, 158, .6);
        }
        .navbar ul li{
            float: left;
            width: 13%;
            height: 10%;
            text-align: center;
            line-height: 47px;
        }
        .navbar a{
            color:#fff;
        }
        .logo{
            width:60%;
            position: absolute;
            z-index: 1;
        }
        .big_bg img{
            height: 21.36%;
            width: 48.73%;
            margin:10% 0 0 32%;
        }
        .intro {
            width: 28vw;
            /*margin-top: 29.6144vh;*/
            margin-left: 27.5316vw;
            font-size: 1.94vh;
            color:#fff;
            text-align: center;
            letter-spacing: 2px;
            margin-top: -7vh;
        }
        @media screen and (max-width:767px){

            .big_bg img{
                height:50%;
                width:90%;
                margin:10% 0 0 6.5%;
            }


            .intro{
                width:70vw;
                margin-top: 5vh;
                margin-left: 20vw;
                font-size: 3vh;

            }


            .letter{
                position: absolute;
                bottom:21vh;
                color: #fff;
                margin:0 23vw 0 23vw;
                line-height:6vh;
                text-align: center;
                font-size: 4vh;
            }
        }
        /*---------------------------------*/
        .mid_bg{
            background: #122b40 ;
            opacity: .8;
        }
        .figure{
            width: 100vw;
            height: 51.6vh;
            position: relative;
        }
        .letter{
            position: absolute;;
            bottom:26vh;
            color: #fff;
            margin:0 23vw 0 23vw;
            line-height:6vh;
            text-align: center;
            font-size: 4vh;
        }
        .color {
            background-color: RGBA(11, 95, 155, .8);
            height: 63vh;
            width: 49.17vw;
        }
        .color:after{
            content: '';
            width: 0;
            height: 0;
            border-right: 200px solid #cfc;
            border-bottom: 100vh solid transparent;
            position: absolute;
            left: 0;
            top: 0;
        }
        .lays{
            background-image: url("img/首頁banner底圖.jpg");
            background-repeat: no-repeat;
            height: 63vh;
            width: 100vw;
            position: relative;
        }
        .lays h2,p{
            color:#f9f2f4;
        }
        .lays h2{
            margin: 0;
        }
        .lays p{
            text-align: center;
        }

    </style>
</head>
<body>
<div class="container-fluid">
    <section class="section1">
<!-----------------------------導覽列---------------------->
        <div class="navbar hidden-xs">
            <ul>
                <li class="col-lg-2"><a href="#">認識極光</a></li>
                <li class="col-lg-2"><a href="#">極光探索</a></li>
                <li class="col-lg-2"><a href="#">極光攝影</a></li>
                <li class="col-lg-2"><a href="#">旅程回顧</a></li>
                <li class="col-lg-2"><a href="#">行程購買</a></li>
                <li class="col-lg-2"><a href="#">會員專區</a></li>
                <li class="col-lg-2"><img src="img/logo_1201-01.png" class="img-responsive logo"></a></li>
            </ul>
        </div>
<!-----------------------------導覽列---------------------->
        <div class="big_bg">
            <img src="img/Index_big_title-01.png" alt="Aurora">
        </div>
        <div class="col-xs-6 intro">
            愛斯基摩人的古老傳說中，極光是先靈為升天者照亮通往天堂之路的火炬；古羅馬時代，人們認為那是戰死後勇士們仍爭鬥不息的刀光劍影；長久以來，極光一直被賦予著幸福的傳說，
            有人說，看到極光的人會幸福一輩子。
        </div>
    </section>



    <section class="section2">
        <div class="figure">
            <div class="mid_bg"><img src="img/首頁冰山底圖.png"></div>
            <h4 class="letter">極光的活躍期以11年為一循環，2016年是這一輪極光活躍期中的最後一年，錯過了今年，想看這麼好的極光，就只能再等11年了。
                一輩子有幾個11年呢？</h4>
        </div>
        <div class="lays">
            <div class="color">
                <h2>Meet The Aurora</h2>
                <p>極光漫遊 x 樂事Lay's</p>
            </div>

        </div>

    </section>
    <section class="section3">


    </section>

</div>

</body>
</html>
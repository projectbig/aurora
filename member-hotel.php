<?php include __DIR__. '/__connect_db.php' ;

$page_name = 'member-hotel';
$hotel_day = isset($_GET['hotel_date']) ? $_GET['hotel_date'] : '';
$room = isset($_GET['room']) ? $_GET['room'] : '';
$breakfast = isset($_GET['breakfast']) ? $_GET['breakfast'] : '';
$smoke = isset($_GET['smoke']) ? $_GET['smoke'] : '';
$room_count = isset($_GET['room_count']) ? $_GET['room_count'] : '';
$people = isset($_GET['people']) ? $_GET['people'] : '';
$hotel_day = '';
$where = " WHERE 1 ";

$room_param = '';
if(! empty($room)) {
    switch ($room) {
        case 'single':
            $where .= " AND `single_onsale` IS NOT NULL ";
            break;
        case 'double':
            $where .= " AND `double_onsale` iS NOT Null ";
            break;
    }
    $room_param = '&room='. $room ;
}

$breakfast_param = '';
if(! empty($breakfast)){
    switch ($breakfast){
        case 'yes':
            $where .= " AND `breakfast` = 'y' ";
            break;
        case 'no':
            $where .= " AND `breakfast` = 'n' ";
            break;
    }
    $breakfast_param = '&breakfast='. $breakfast ;
}

$sql = "SELECT * FROM `hotel` $where";
$result = $mysqli->query($sql);
$row = $result->fetch_assoc();

echo "$sql <br>";
echo print_r($result) ;



?>
<?php include __DIR__. '/__html_head.php' ?>
<div class="container-fluid">
    <?php include __DIR__. '/__navbar.php' ?>
    <div class="container" style="margin-top: 60px">



        <div class="col-lg-3">
            <form name="form1" method="get">
                <label for="hotel_date">入住日期：</label><br>
                <input type="date" name="hotel_date" id="hotel_date" value="<?= $hotel_day ?>" style="width: 100%"
                       min="<?= date('Y-m-d') ?>">
        </div>


        <div class="col-lg-3">
            房型選擇<br>
                <form action="">
                    <div class="form-group">
                        <select name="room" id="room" class="form-control">
                            <option value="">請選擇房型</option>
                            <option value="single">單人房</option>
                            <option value="double">雙人房</option>
                        </select>
                    </div>
                </form>
        </div>

        <div class="col-lg-3">
            早餐<br>
            <form action="">
                <div class="form-group">
                    <select name="breakfast" id="breakfast" class="form-control">
                        <option value=""></option>
                        <option value="yes">有</option>
                        <option value="no">無</option>
                    </select>
                </div>
            </form>
        </div>

        <div class="col-lg-3">
            吸菸房<br>
            <form action="">
                <div class="form-group">
                    <select name="smoke" id="smoke" class="form-control">
                        <option value=""></option>
                        <option value="yes">有</option>
                        <option value="no">無</option>
                    </select>
                </div>
            </form>
        </div>

        <div class="col-lg-3">
            總間數<br>
            <form action="">
                <div class="form-group">
                    <select name="room_count" id="room_count" class="form-control">
                        <option value=""></option>
                        <?php for($i=1; $i<11 ; $i++):?>
                        <option value="<?= $i ?>"><?= $i ?>間</option>
                        <?php endfor; ?>
                    </select>
                </div>
            </form>
        </div>

        <div class="col-lg-3">
            入住晚數<br>
            <form action="">
                <div class="form-group">
                    <select name="people" id="people" class="form-control">
                        <option value=""></option>
                        <?php for($i=1; $i<11 ; $i++):?>
                            <option value="<?= $i ?>"><?= $i ?>晚</option>
                        <?php endfor; ?>
                    </select>
                </div>
            </form>
        </div>

        <div class="bs-example" data-example-id="striped-table"  style="margin:50px 0">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>飯店名稱</th>
                    <th>飯店星等</th>
                    <th>地理位置</th>
                    <th>價格/每晚</th>
                    <th>選取</th>
                </tr>
                </thead>
                <tbody>
                <?php $result-> data_seek(0)?>
                <?php while($row = $result->fetch_assoc()): ?>
                    <tr>
                        <td><?= $row['hotel_name'] ?></td>
                        <td><?= $row['starts'] ?></td>
                        <td><?= $row['locat_sid'] ?></td>
                        <td><?= $row['room'] ?></td>
                        <td>
                            <input type="radio" value="" class="take">
                        </td>
                    </tr>
                <?php endwhile; ?>
                </tbody>
            </table>
            <button type="button" class="btn btn-default check" data-sid="<?= $row['sid'] ?>"><a href="">確認</a></button>
        </div>



    </div>
</div>

<script>

    var params = <?= json_encode($_GET) ?>;
    var qs = '';


    $('#room').on('change', function() {
        //alert( $(this).val() );

        qs = '?room=' + $(this).val();

        if(params.breakfast) {
            $('#breakfast').val(params.breakfast);
            qs += '&breakfast>=' +$('#breakfast').val();
        }

        if (params.smoke) {
            $('#smoke').val( params.smoke );
            qs += '&smoke=' + $('#smoke').val();
        }
        if (params.room_count) {
            $('#room_count').val( params.room_count );
            qs += '&room_count=' + $('#room_count').val();
        }
        if (params.people) {
            $('#people').val( params.people );
            qs += '&people=' + $('#people').val();
        }
        location.href = qs;
    });
    if(params.room){
        $('#room').val( params.room );
    }
    //**********************************************************

    $('#breakfast').on('change', function() {
        qs = '?breakfast=' + $(this).val();

        if(params.room) {
            $('#room').val(params.room);
            qs += '&room>=' +$('#room').val();
        }
        if (params.smoke) {
            $('#smoke').val( params.smoke );
            qs += '&smoke=' + $('#smoke').val();
        }
        if (params.room_count) {
            $('#room_count').val( params.room_count );
            qs += '&room_count=' + $('#room_count').val();
        }
        if (params.people) {
            $('#people').val( params.people );
            qs += '&people=' + $('#people').val();
        }
        location.href = qs;
    });
    if(params.breakfast){
        $('#breakfast').val( params.breakfast );
    }


    //**********************************************************

    $('#depature').on('change', function(){
        qs = '?depature=' + $(this).val();

        if(params.go) {
            $('#go').val(params.go);
            qs += '&go>=' +$('#go').val();
        }
        if(params.airline_name){
            $('#airline_name').val( params.airline_name );
            qs += '&airline_name=' + $('#airline_name').val();
        }
        if(params.seat){
            $('#seat').val( params.seat );
            qs += '&seat=' + $('#seat').val();
        }
        if(params.arrival){
            $('#arrival').val( params.arrival );
            qs += '&arrival=' + $('#arrival').val();
        }
        location.href = qs ;
    });
    if(params.depature){
        $('#depature').val( params.depature );
    }
    //*********************************************************
    $('#arrival').on('change', function(){
        qs = '?arrival=' + $(this).val();

        if(params.go) {
            $('#go').val(params.go);
            qs += '&go>=' +$('#go').val();
        }
        if(params.airline_name){
            $('#airline_name').val( params.airline_name );
            qs += '&airline_name=' + $('#airline_name').val();
        }
        if(params.seat){
            $('#seat').val( params.seat );
            qs += '&seat=' + $('#seat').val();
        }
        if(params.depature){
            qs += '&depature=' + $('#depature').val();
        }
        location.href = qs ;
    });
    if(params.arrival){
        $('#arrival').val( params.arrival );
    }
    //**********************************************************

</script>
